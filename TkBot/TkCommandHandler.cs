using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using Commands;

namespace CommandHandler{

public class TkCommandHandler {
    private readonly DiscordSocketClient _client;
    private readonly CommandService _commands;
    private readonly IServiceProvider _services;
    public TkCommandHandler(DiscordSocketClient client, CommandService commands, IServiceProvider services){

        _commands = commands;
        _client = client;
        _services = services;
    }

    public async Task HandleCommandAsync(SocketMessage arg){
        
        var message = arg as SocketUserMessage;

        var context = new SocketCommandContext(_client, message);
        if (message.Author.IsBot) return;

        int argPos = 0;
        if (message.HasStringPrefix("!", ref argPos))
        {
            Console.WriteLine(message);
            var result = await _commands.ExecuteAsync(context, argPos, _services);
            if (!result.IsSuccess) Console.WriteLine(result.ErrorReason);
            if (result.Error.Equals(CommandError.UnmetPrecondition)) await message.Channel.SendMessageAsync(result.ErrorReason);
        }

    }
}
}