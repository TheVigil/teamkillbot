using Discord.Commands;
using Discord.Interactions;

namespace Commands{
    public class CommandModule : ModuleBase<SocketCommandContext>{

        [Command("ping")]
        public async Task Ping()
        {
            await ReplyAsync("pong");
        }

        [Command("scores")]
        public async Task Scores()
        {
            // TODO: print the tk scores of all members on the board 
            return;
        }

        [Command("teamkill")]
        public async Task Teamkill()
        {
            // TODO: register a new teamkill to the board
            return;
        }

        [Command("player")]
        public async Task Player()
        {
            // TODO:  print the tk score of a specific member of the board
            return;

        }

        [Command("Help")]
        public async Task Help()
        {
            // TODO: print a help message detailing the commands the TKBot can understand
        }
    }
}