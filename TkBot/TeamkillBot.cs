﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using CommandHandler;


namespace TeamKillBot{

    public class TeamKillBot
    {
        static void Main(string[] args0) => new TeamKillBot().RunTeamKillAysnc().GetAwaiter().GetResult();

        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        private TkCommandHandler _handler;

        public async Task RunTeamKillAysnc()
        {
            _client = new DiscordSocketClient();
            _commands = new CommandService();
            _services = new ServiceCollection()
                .AddSingleton(_client)
                .AddSingleton(_commands)
                .BuildServiceProvider();
                
            _handler = new TkCommandHandler(_client, _commands, _services);

            string token = "OTc3OTA5NjUwNzU2MzU0MTM5.GTIoTz.rOy3E-OOBD6Vcu6ccregF3nDVz2dSS26F1OqX8";

            _client.Log += ClientLog;

            await RegisterCommandsAsync();
            await _client.LoginAsync(TokenType.Bot, token);
            await _client.StartAsync();
            await Task.Delay(-1);

        }

        private Task ClientLog(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        private async Task RegisterCommandsAsync()
        {

            _client.MessageReceived += _handler.HandleCommandAsync;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        

    }

}